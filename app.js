console.log(' ______    _________  _____    ')
console.log('|_   _ `. |  _   _  ||_   _|   ')
console.log('  | | `. \\|_/ | | \\_|  | |   ')
console.log('  | |  | |    | |      | |     ')
console.log(' _| |_.` /   _| |_    _| |_    ')
console.log('|______.`   |_____|  |_____|   ')
var nodemailer = require('nodemailer')
var dateFormat = require('dateformat')
const pug = require('pug')
var fs = require('fs')
/** Arquivos de propriedades
 * As informações de configurações de email e modo estão em arquivos .json localizadas no diretório resources:
 * O arquivo updationjs.json informa se o ambiente está de teste 'dev' ou produção 'prd.
 *
 * ***********VERIFIQUE O UPDATION.JS ANTES DE REALIZAR QUALQUER TESTE************
 * */
const info = function () {
  return JSON.parse(fs.readFileSync('./resources/updationjs.json'))
}

const idxExtensao = function (prp, argv) {
  return prp.extensoesValidas.indexOf(objArquivo(argv).extensao)
}

const extensoesToString = function (prp) {
  return 'extensão(ões)  ' + prp.extensoesValidas.join(', ').replace('|', ', ')
}
const ujsProps = info()
const execucao = ujsProps.ambiente

var carga = (arq) => JSON.parse(fs.readFileSync('./resources/'.concat(arq).concat('.').concat(execucao.toLowerCase()).concat('.json')))

var prp = carga('props')
var smtp = carga('smtp')
var strAmb = 'DESENVOLVIMENTO'
if (execucao.toLowerCase() === 'prd') {
  strAmb = 'PRODUÇÃO       '
}
console.log('+---------------------------------------------------+    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('|        AMBIENTE DE ' + strAmb + '                |    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('|                                                   |    ')
console.log('+---------------------------------------------------+   ')
// Para gerar um objeto com as informações do arquivo:
var objArquivo = function (str) {
  var arrFile = str.split('\\')
  var nome = arrFile.pop()
  var ext = nome.substr(nome.indexOf('.') + 1)
  return {
    nome: nome,
    caminho: str,
    extensao: ext
  }
}

var idxTipo = -1
for (let i = 2; i < process.argv.length; i++) {
  idxTipo = idxExtensao(prp, process.argv[i])
  if (idxTipo < 0) {
    console.error('TIPO DE ARQUIVO INVÁLIDO - somente arquivos com ' + extensoesToString(prp) + ' são aceitos neste processo!')
    console.error('Arquivo com falha: ' + objArquivo(process.argv[i]).nome)
    console.info('Encerrando aplicação...')
    process.exit(1)
  }
}
let opts = {
  assinaturaNome: ujsProps.nomeAssinatura,
  assinaturaDepto: ujsProps.deptoAssinatura,
  assinaturaFuncao: ujsProps.funcaoAssinatura,
  assinaturaTelefone: ujsProps.telefoneAssinatura,
  hoje: dateFormat(new Date(), 'dd/mm/yyyy'),
  agora: dateFormat(new Date(), 'HH:MM:ss'),
  copia: prp.mailDir[idxTipo],
  tipo: prp.tipos[idxTipo],
  listaClasses: ' ',
  sistemas: prp.sistemas.join(','),
  ambiente: ((strAmb === 'PRODUÇÃO       ') ? 'PRODUCAO' : 'TESTE')
}
for (let i = 2; i < process.argv.length; i++) {
  let val = process.argv[i]
  let arquivo = objArquivo(val)
  console.log('Copiando '.concat(arquivo.nome).concat('...'))
  let stream = fs.readFileSync(arquivo.caminho)
  fs.writeFileSync(prp.dir[idxTipo].concat(arquivo.nome), stream)
  opts.listaClasses = opts.listaClasses.concat(arquivo.nome).concat(' ')
}
let formatado = pug.renderFile('./emails/carteiro/html.pug', opts)
let assunto = pug.renderFile('./emails/carteiro/subject.pug', opts)
let texto = pug.renderFile('./emails/carteiro/texto.pug', opts)
let smtpTransport = nodemailer.createTransport(smtp)

// setup e-mail data with unicode symbols
var mailOptions = {
  from: prp.de.concat(' <').concat(prp.from).concat('>'),
  replyTo: prp.replyTo,
  to: prp.to,
  cc: prp.cc,
  subject: assunto,
  text: texto,
  html: formatado
}

smtpTransport.sendMail(mailOptions, (error, response) => {
  if (error) {
    console.log(error)
  } else {
    console.log('Processo concluído!')
  }
  smtpTransport.close()
})
